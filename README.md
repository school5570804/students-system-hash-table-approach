<!-- CONSIDERATIONS TAKEN WHEN CREATING THIS CODE -->
1. For the data structure I used a hash table, the purpose for this was to achieve constant time when searching for a student such that I do not spend the I do not have to loop through all the student records

2. While inserting, i added linked lists in each row of the body so as to handle collisions. 
        The hash function helps me determine where in the table my new student will be stored.
        In case of a collision then the new student would just be prepended to the linked list.